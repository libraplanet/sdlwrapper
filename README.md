# これは？ #
SDL2の.NETラッパーです。
個人的に必要最低限のものを実装したものです。

## 開発環境 ##
* Windows 7/10
* Visual Studio 2013 for Desktop
* C#
* SDL2


## 謝辞 ##
* SDL [Simple DirectMedia Layer](https://www.libsdl.org/)
* SDL2 C# Binder [flibitijibibo/SDL2-CS · GitHub](https://github.com/flibitijibibo/SDL2-CS)
* SDL.NET [C# SDL](http://cs-sdl.sourceforge.net/)


# License #
MIT License

Copyright (c) 2015 takumi



Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:



The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.



THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.