﻿#define SDL_VERSION_ATLEAST
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace SdlWrapper
{
    public static class SdlImage
    {
        private const string DLL = "SDL2_image.dll";
        private const CallingConvention CALL = CallingConvention.Cdecl;
        private const CharSet CHARSET = CharSet.Unicode;

        //public static extern const SDL_version * IMG_Linked_Version();

        public const int IMG_INIT_JPG = 0x00000001;
        public const int IMG_INIT_PNG = 0x00000002;
        public const int IMG_INIT_TIF = 0x00000004;
        public const int IMG_INIT_WEBP = 0x00000008;
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int IMG_Init(int flags);

        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern void IMG_Quit();

        #region [(SDL_Surface *)]

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <returns>(SDL_Surface *)</returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern IntPtr IMG_Load([In()] [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef = typeof(LPUtf8StrMarshaler))] string file);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="src">(SDL_RWops *)</param>
        /// <param name="freesrc"></param>
        /// <param name="type">"BMP", "GIF", "PNG", and more... </param>
        /// <returns>(SDL_Surface *)</returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern IntPtr IMG_LoadTyped_RW(IntPtr src, int freesrc, [In()] [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef = typeof(LPUtf8StrMarshaler))] string type);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="src">(SDL_RWops *)</param>
        /// <param name="freesrc"></param>
        /// <returns>(SDL_Surface *)</returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern IntPtr IMG_Load_RW(IntPtr src, int freesrc);
        #endregion

        #region [(SDL_Texture *)]
        #if SDL_VERSION_ATLEAST
        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderer">(SDL_Renderer *)</param>
        /// <param name="file"></param>
        /// <returns>(SDL_Texture *)</returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern IntPtr IMG_LoadTexture(IntPtr renderer, [In()] [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef = typeof(LPUtf8StrMarshaler))] string file);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderer">(SDL_Renderer *)</param>
        /// <param name="src">(SDL_RWops *)</param>
        /// <param name="freesrc"></param>
        /// <returns>(SDL_Texture *)</returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern IntPtr IMG_LoadTexture_RW(IntPtr renderer, IntPtr src, int freesrc);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderer">(SDL_Renderer *)</param>
        /// <param name="src">(SDL_RWops *)</param>
        /// <param name="freesrc"></param>
        /// <param name="type"></param>
        /// <returns>(SDL_Texture *)</returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern IntPtr IMG_LoadTextureTyped_RW(IntPtr renderer, IntPtr src, int freesrc, [In()] [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef = typeof(LPUtf8StrMarshaler))] string type);
        #endif
        #endregion

        /* Functions to detect a file type, given a seekable source */
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int IMG_isICO(IntPtr src);
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int IMG_isCUR(IntPtr src);
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int IMG_isBMP(IntPtr src);
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int IMG_isGIF(IntPtr src);
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int IMG_isJPG(IntPtr src);
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int IMG_isLBM(IntPtr src);
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int IMG_isPCX(IntPtr src);
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int IMG_isPNG(IntPtr src);
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int IMG_isPNM(IntPtr src);
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int IMG_isTIF(IntPtr src);
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int IMG_isXCF(IntPtr src);
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int IMG_isXPM(IntPtr src);
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int IMG_isXV(IntPtr src);
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int IMG_isWEBP(IntPtr src);

        #region [(SDL_Surface *)]
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern IntPtr IMG_LoadICO_RW(IntPtr src);
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern IntPtr IMG_LoadCUR_RW(IntPtr src);
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern IntPtr IMG_LoadBMP_RW(IntPtr src);
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern IntPtr IMG_LoadGIF_RW(IntPtr src);
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern IntPtr IMG_LoadJPG_RW(IntPtr src);
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern IntPtr IMG_LoadLBM_RW(IntPtr src);
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern IntPtr IMG_LoadPCX_RW(IntPtr src);
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern IntPtr IMG_LoadPNG_RW(IntPtr src);
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern IntPtr IMG_LoadPNM_RW(IntPtr src);
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern IntPtr IMG_LoadTGA_RW(IntPtr src);
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern IntPtr IMG_LoadTIF_RW(IntPtr src);
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern IntPtr IMG_LoadXCF_RW(IntPtr src);
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern IntPtr IMG_LoadXPM_RW(IntPtr src);
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern IntPtr IMG_LoadXV_RW(IntPtr src);
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern IntPtr IMG_LoadWEBP_RW(IntPtr src);

        //[DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        //public static extern IntPtr IMG_ReadXPMFromArray(char** xpm);
        #endregion

        #region [save png]
        //public static extern int IMG_SavePNG(IntPtr surface, const char *file);
        //public static extern int IMG_SavePNG_RW(IntPtr surface, IntPtr dst, int freedst);
        #endregion
    }
}
