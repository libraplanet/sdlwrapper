﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace SdlWrapper
{
    public static class Sdl
    {
        private const string DLL = "SDL2.dll";
        private const CallingConvention CALL = CallingConvention.Cdecl;
        private const CharSet CHARSET = CharSet.Unicode;

#if SDL_BIG_ENDIAN
        public const UInt32 RMASK = 0xff000000;
        public const UInt32 GMASK = 0x00ff0000;
        public const UInt32 BMASK = 0x0000ff00;
        public const UInt32 AMASK = 0x000000ff;
#else
        public const UInt32 RMASK = 0x000000ff;
        public const UInt32 GMASK = 0x0000ff00;
        public const UInt32 BMASK = 0x00ff0000;
        public const UInt32 AMASK = 0xff000000;
#endif

        #region [SDL.h]
        public const UInt32 SDL_INIT_TIMER = 0x00000001;
        public const UInt32 SDL_INIT_AUDIO = 0x00000010;
        /// <summary>
        /// SDL_INIT_VIDEO implies SDL_INIT_EVENTS
        /// </summary>
        public const UInt32 SDL_INIT_VIDEO = 0x00000020;
        /// <summary>
        /// SDL_INIT_JOYSTICK implies SDL_INIT_EVENTS
        /// </summary>
        public const UInt32 SDL_INIT_JOYSTICK = 0x00000200;
        public const UInt32 SDL_INIT_HAPTIC = 0x00001000;
        /// <summary>
        /// SDL_INIT_GAMECONTROLLER implies SDL_INIT_JOYSTICK
        /// </summary>
        public const UInt32 SDL_INIT_GAMECONTROLLER = 0x00002000;
        public const UInt32 SDL_INIT_EVENTS = 0x00004000;
        /// <summary>
        /// Don't catch fatal signals
        /// </summary>
        public const UInt32 SDL_INIT_NOPARACHUTE = 0x00100000;
        public const UInt32 SDL_INIT_EVERYTHING = (SDL_INIT_TIMER | SDL_INIT_AUDIO | SDL_INIT_VIDEO | SDL_INIT_EVENTS |
            SDL_INIT_JOYSTICK | SDL_INIT_HAPTIC | SDL_INIT_GAMECONTROLLER);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="flags">SDL_INIT_*</param>
        /// <returns>result</returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int SDL_Init(UInt32 flags);
        #endregion

        #region [SDL_stdinc.h]
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        internal static extern IntPtr SDL_malloc(IntPtr size);

        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        internal static extern void SDL_free(IntPtr memblock);
        #endregion

        #region [SDL_video.h]
        [Flags]
        public enum SDL_WindowFlags : uint
        {
            SDL_WINDOW_FULLSCREEN = 0x00000001,
            SDL_WINDOW_OPENGL = 0x00000002,
            SDL_WINDOW_SHOWN = 0x00000004,
            SDL_WINDOW_HIDDEN = 0x00000008,
            SDL_WINDOW_BORDERLESS = 0x00000010,
            SDL_WINDOW_RESIZABLE = 0x00000020,
            SDL_WINDOW_MINIMIZED = 0x00000040,
            SDL_WINDOW_MAXIMIZED = 0x00000080,
            SDL_WINDOW_INPUT_GRABBED = 0x00000100,
            SDL_WINDOW_INPUT_FOCUS = 0x00000200,
            SDL_WINDOW_MOUSE_FOCUS = 0x00000400,
            SDL_WINDOW_FULLSCREEN_DESKTOP =
                (SDL_WINDOW_FULLSCREEN | 0x00001000),
            SDL_WINDOW_FOREIGN = 0x00000800,
            SDL_WINDOW_ALLOW_HIGHDPI = 0x00002000	/* Only available in 2.0.1 */
        }

        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern IntPtr SDL_CreateWindow([In()] [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef = typeof(LPUtf8StrMarshaler))] string title, int x, int y, int w,int h, UInt32 flags);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hWnd">HWND</param>
        /// <returns>(SDL_Window *)</returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern IntPtr SDL_CreateWindowFrom(IntPtr hWnd);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="window">(SDL_Window *)</param>
        /// <returns>(SDL_Surface *)</returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern IntPtr SDL_GetWindowSurface(IntPtr window);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="window">(SDL_Window *)</param>
        /// <returns>(SDL_Surface *)</returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern void SDL_DestroyWindow(IntPtr window);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="window">(SDL_Window *)</param>
        /// <returns>result</returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int SDL_UpdateWindowSurface(IntPtr window);
        #endregion

        #region [SDL_rect.h]
        [StructLayout(LayoutKind.Sequential)]
        public struct SDL_Point
        {
            public int x;
            public int y;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct SDL_Rect
        {
            public int x, y;
            public int w, h;
        }
        #endregion

        #region [SDL_render.h]

        public const UInt32 SDL_RENDERER_SOFTWARE = 0x00000001;
        public const UInt32 SDL_RENDERER_ACCELERATED = 0x00000002;
        public const UInt32 SDL_RENDERER_PRESENTVSYNC = 0x00000004;
        public const UInt32 SDL_RENDERER_TARGETTEXTURE = 0x00000008;

        public const UInt32 SDL_TEXTUREMODULATE_NONE = 0x00000000;
        public const UInt32 SDL_TEXTUREMODULATE_COLOR = 0x00000001;
        public const UInt32 SDL_TEXTUREMODULATE_ALPHA = 0x00000002;

        public const UInt32 SDL_FLIP_NONE = 0x00000000;
        public const UInt32 SDL_FLIP_HORIZONTAL = 0x00000001;
        public const UInt32 SDL_FLIP_VERTICAL = 0x00000002;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="window">(SDL_Window *)</param>
        /// <param name="index">num or -1</param>
        /// <param name="flags">SDL_RENDERE*</param>
        /// <returns>(SDL_Render *)</returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern IntPtr SDL_CreateRenderer(IntPtr window, int index, UInt32 flags);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="surface">(SDL_Surface *)</param>
        /// <returns>(SDL_Render *)</returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern IntPtr SDL_CreateSoftwareRenderer(IntPtr surface);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="surface">(SDL_Window *)</param>
        /// <returns>(SDL_Render *)</returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern IntPtr SDL_GetRenderer(IntPtr window);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderer">(SDL_Render *)</param>
        /// <param name="surface">(SDL_Surface *)</param>
        /// <returns>(SDL_Texture *)</returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern IntPtr SDL_CreateTextureFromSurface(IntPtr renderer, IntPtr surface);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderer">(SDL_Render *)</param>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern void SDL_RenderPresent(IntPtr renderer);
 
        /// <summary>
        /// 
        /// </summary>
        /// <param name="texture">(SDL_Texture *)</param>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern void SDL_DestroyTexture(IntPtr texture);
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderer">(SDL_Render *)</param>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern void SDL_DestroyRenderer(IntPtr renderer);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderer">(SDL_Render *)</param>
        /// <param name="w"></param>
        /// <param name="h"></param>
        /// <returns></returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int SDL_RenderSetLogicalSize(IntPtr renderer, int w, int h);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderer">(SDL_Render *)</param>
        /// <param name="w"></param>
        /// <param name="h"></param>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern void SDL_RenderGetLogicalSize(IntPtr renderer, [Out] int w, [Out] int h);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderer">(SDL_Render *)</param>
        /// <param name="scaleX"></param>
        /// <param name="scaleY"></param>
        /// <returns></returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int SDL_RenderSetScale(IntPtr renderer, float scaleX, float scaleY);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderer">(SDL_Render *)</param>
        /// <param name="scaleX"></param>
        /// <param name="scaleY"></param>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern void SDL_RenderGetScale(IntPtr renderer, [Out]float scaleX, [Out] float scaleY);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderer">(SDL_Render *)</param>
        /// <param name="texture">(SDL_Texture *)</param>
        /// <param name="srcrect">(SDL_Rect *)</param>
        /// <param name="dstrect">(SDL_Rect *)</param>
        /// <returns>0 on success, or -1 on error</returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int SDL_RenderCopy(IntPtr renderer, IntPtr texture, IntPtr srcrect, IntPtr dstrect);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderer">(SDL_Render *)</param>
        /// <param name="texture">(SDL_Texture *)</param>
        /// <param name="srcrect">(SDL_Rect *)</param>
        /// <param name="dstrect">(SDL_Rect *)</param>
        /// <param name="angle"></param>
        /// <param name="center"></param>
        /// <param name="flip">(SDL_FLIP_*)</param>
        /// <returns>0 on success, or -1 on error</returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int SDL_RenderCopyEx(IntPtr renderer, IntPtr texture, IntPtr srcrect, IntPtr dstrect, double angle, IntPtr center, UInt32 flip);

        #endregion

        #region [SDL_surface.h]
        /// <summary>
        /// Just here for compatibility
        /// </summary>
        public const UInt32 SDL_SWSURFACE = 0;
        /// <summary>
        /// Surface uses preallocated memory
        /// </summary>
        public const UInt32 SDL_PREALLOC = 0x00000001;
        /// <summary>
        /// Surface is RLE encoded
        /// </summary>
        public const UInt32 SDL_RLEACCEL = 0x00000002;
        /// <summary>
        /// Surface is referenced internally
        /// </summary>
        public const UInt32 SDL_DONTFREE = 0x00000004;

        [StructLayout(LayoutKind.Sequential)]
        public struct SDL_Surface
        {
            public UInt32 flags;
            
            /// <summary>
            /// (SDL_PixelFormat *)
            /// </summary>
            public IntPtr format;    /**< Read-only */

            public int w;
            public int h;
            public int pitch;

            /// <summary>
            /// (void *)
            /// </summary>
            public IntPtr pixels;

            /// <summary>
            /// (void *)
            /// </summary>
            public IntPtr userdata;

            public int locked;

            public IntPtr lock_data;

            public SDL_Rect clip_rect;
            
            /// <summary>
            /// (SDL_BlitMap *)
            /// </summary>
            public IntPtr map;

            public int refcount;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="flags"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="depth"></param>
        /// <param name="Rmask"></param>
        /// <param name="Gmask"></param>
        /// <param name="Bmask"></param>
        /// <param name="Amask"></param>
        /// <returns>(SDL_Rect *)</returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern IntPtr SDL_CreateRGBSurface(UInt32 flags, int width, int height, int depth, UInt32 Rmask, UInt32 Gmask, UInt32 Bmask, UInt32 Amask);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="pixels"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="depth"></param>
        /// <param name="pitch"></param>
        /// <param name="Rmask"></param>
        /// <param name="Gmask"></param>
        /// <param name="Bmask"></param>
        /// <param name="Amask"></param>
        /// <returns>(SDL_Rect *)</returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern IntPtr SDL_CreateRGBSurfaceFrom(IntPtr pixels, int width, int height, int depth, int pitch, UInt32 Rmask, UInt32 Gmask, UInt32 Bmask, UInt32 Amask);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="surface">(SDL_Rect *)</param>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern void SDL_FreeSurface(IntPtr surface);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dstSurface">(SDL_Surface *)</param>
        /// <param name="rect">(SDL_Rect *)</param>
        /// <param name="color"></param>
        /// <returns></returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int SDL_FillRect(IntPtr dstSurface, ref SDL_Rect rect, UInt32 color);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dstSurface">(SDL_Surface *)</param>
        /// <param name="rect">(SDL_Rect *)</param>
        /// <param name="color"></param>
        /// <returns></returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int SDL_FillRect(IntPtr dstSurface, IntPtr rect, UInt32 color);

        #region [SDL_Blit]

        /// <summary>
        /// 
        /// </summary>
        /// <param name="srcSurface">(SDL_Surface *)</param>
        /// <param name="srcrect">(SDL_Rect *)</param>
        /// <param name="dstSurface">(SDL_Surface *)</param>
        /// <param name="dstrect">(SDL_Rect *)</param>
        /// <returns></returns>
        [DllImport(DLL, EntryPoint = "SDL_UpperBlit", CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int SDL_Blit(IntPtr srcSurface, IntPtr srcrect, IntPtr dstSurface, IntPtr dstrect);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="srcSurface">(SDL_Surface *)</param>
        /// <param name="srcrect">(SDL_Rect *)</param>
        /// <param name="dstSurface">(SDL_Surface *)</param>
        /// <param name="dstrect">(SDL_Rect *)</param>
        /// <returns></returns>
        [DllImport(DLL, EntryPoint = "SDL_UpperBlit", CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int SDL_Blit(IntPtr srcSurface, IntPtr srcrect, IntPtr dstSurface, ref SDL_Rect dstrect);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="srcSurface">(SDL_Surface *)</param>
        /// <param name="srcrect">(SDL_Rect *)</param>
        /// <param name="dstSurface">(SDL_Surface *)</param>
        /// <param name="dstrect">(SDL_Rect *)</param>
        /// <returns></returns>
        [DllImport(DLL, EntryPoint = "SDL_UpperBlit", CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int SDL_Blit(IntPtr srcSurface, ref SDL_Rect srcrect, IntPtr dstSurface, IntPtr dstrect);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="srcSurface">(SDL_Surface *)</param>
        /// <param name="srcrect">(SDL_Rect *)</param>
        /// <param name="dstSurface">(SDL_Surface *)</param>
        /// <param name="dstrect">(SDL_Rect *)</param>
        /// <returns></returns>
        [DllImport(DLL, EntryPoint = "SDL_UpperBlit", CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int SDL_Blit(IntPtr srcSurface, ref SDL_Rect srcrect, IntPtr dstSurface, ref SDL_Rect dstrect);

        #endregion

        #region [SDL_BlitScaled]

        /// <summary>
        /// 
        /// </summary>
        /// <param name="srcSurface">(SDL_Surface *)</param>
        /// <param name="srcrect">(SDL_Rect *)</param>
        /// <param name="dstSurface">(SDL_Surface *)</param>
        /// <param name="dstrect">(SDL_Rect *)</param>
        /// <returns></returns>
        [DllImport(DLL, EntryPoint = "SDL_UpperBlitScaled", CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int SDL_BlitScaled(IntPtr srcSurface, IntPtr srcrect, IntPtr dstSurface, IntPtr dstrect);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="srcSurface">(SDL_Surface *)</param>
        /// <param name="srcrect">(SDL_Rect *)</param>
        /// <param name="dstSurface">(SDL_Surface *)</param>
        /// <param name="dstrect">(SDL_Rect *)</param>
        /// <returns></returns>
        [DllImport(DLL, EntryPoint = "SDL_UpperBlitScaled", CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int SDL_BlitScaled(IntPtr srcSurface, IntPtr srcrect, IntPtr dstSurface, ref SDL_Rect dstrect);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="srcSurface">(SDL_Surface *)</param>
        /// <param name="srcrect">(SDL_Rect *)</param>
        /// <param name="dstSurface">(SDL_Surface *)</param>
        /// <param name="dstrect">(SDL_Rect *)</param>
        /// <returns></returns>
        [DllImport(DLL, EntryPoint = "SDL_UpperBlitScaled", CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int SDL_BlitScaled(IntPtr srcSurface, ref SDL_Rect srcrect, IntPtr dstSurface, IntPtr dstrect);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="srcSurface">(SDL_Surface *)</param>
        /// <param name="srcrect">(SDL_Rect *)</param>
        /// <param name="dstSurface">(SDL_Surface *)</param>
        /// <param name="dstrect">(SDL_Rect *)</param>
        /// <returns></returns>
        [DllImport(DLL, EntryPoint = "SDL_UpperBlitScaled", CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int SDL_BlitScaled(IntPtr srcSurface, ref SDL_Rect srcrect, IntPtr dstSurface, ref SDL_Rect dstrect);

        #endregion

        #region [SDL_UpperBlit]

        /// <summary>
        /// 
        /// </summary>
        /// <param name="srcSurface">(SDL_Surface *)</param>
        /// <param name="srcrect">(SDL_Rect *)</param>
        /// <param name="dstSurface">(SDL_Surface *)</param>
        /// <param name="dstrect">(SDL_Rect *)</param>
        /// <returns></returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int SDL_UpperBlit(IntPtr srcSurface, IntPtr srcrect, IntPtr dstSurface, IntPtr dstrect);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="srcSurface">(SDL_Surface *)</param>
        /// <param name="srcrect">(SDL_Rect *)</param>
        /// <param name="dstSurface">(SDL_Surface *)</param>
        /// <param name="dstrect">(SDL_Rect *)</param>
        /// <returns></returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int SDL_UpperBlit(IntPtr srcSurface, IntPtr srcrect, IntPtr dstSurface, ref SDL_Rect dstrect);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="srcSurface">(SDL_Surface *)</param>
        /// <param name="srcrect">(SDL_Rect *)</param>
        /// <param name="dstSurface">(SDL_Surface *)</param>
        /// <param name="dstrect">(SDL_Rect *)</param>
        /// <returns></returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int SDL_UpperBlit(IntPtr srcSurface, ref SDL_Rect srcrect, IntPtr dstSurface, IntPtr dstrect);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="srcSurface">(SDL_Surface *)</param>
        /// <param name="srcrect">(SDL_Rect *)</param>
        /// <param name="dstSurface">(SDL_Surface *)</param>
        /// <param name="dstrect">(SDL_Rect *)</param>
        /// <returns></returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int SDL_UpperBlit(IntPtr srcSurface, ref SDL_Rect srcrect, IntPtr dstSurface, ref SDL_Rect dstrect);

        #endregion

        #region [SDL_UpperBlitScaled]

        /// <summary>
        /// 
        /// </summary>
        /// <param name="srcSurface">(SDL_Surface *)</param>
        /// <param name="srcrect">(SDL_Rect *)</param>
        /// <param name="dstSurface">(SDL_Surface *)</param>
        /// <param name="dstrect">(SDL_Rect *)</param>
        /// <returns></returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int SDL_UpperBlitScaled(IntPtr srcSurface, IntPtr srcrect, IntPtr dstSurface, IntPtr dstrect);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="srcSurface">(SDL_Surface *)</param>
        /// <param name="srcrect">(SDL_Rect *)</param>
        /// <param name="dstSurface">(SDL_Surface *)</param>
        /// <param name="dstrect">(SDL_Rect *)</param>
        /// <returns></returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int SDL_UpperBlitScaled(IntPtr srcSurface, IntPtr srcrect, IntPtr dstSurface, ref SDL_Rect dstrect);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="srcSurface">(SDL_Surface *)</param>
        /// <param name="srcrect">(SDL_Rect *)</param>
        /// <param name="dstSurface">(SDL_Surface *)</param>
        /// <param name="dstrect">(SDL_Rect *)</param>
        /// <returns></returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int SDL_UpperBlitScaled(IntPtr srcSurface, ref SDL_Rect srcrect, IntPtr dstSurface, IntPtr dstrect);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="srcSurface">(SDL_Surface *)</param>
        /// <param name="srcrect">(SDL_Rect *)</param>
        /// <param name="dstSurface">(SDL_Surface *)</param>
        /// <param name="dstrect">(SDL_Rect *)</param>
        /// <returns></returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int SDL_UpperBlitScaled(IntPtr srcSurface, ref SDL_Rect srcrect, IntPtr dstSurface, ref SDL_Rect dstrect);

        #endregion

        #region [SDL_LowerBlit]

        /// <summary>
        /// 
        /// </summary>
        /// <param name="srcSurface">(SDL_Surface *)</param>
        /// <param name="srcrect">(SDL_Rect *)</param>
        /// <param name="dstSurface">(SDL_Surface *)</param>
        /// <param name="dstrect">(SDL_Rect *)</param>
        /// <returns></returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int SDL_LowerBlit(IntPtr srcSurface, IntPtr srcrect, IntPtr dstSurface, IntPtr dstrect);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="srcSurface">(SDL_Surface *)</param>
        /// <param name="srcrect">(SDL_Rect *)</param>
        /// <param name="dstSurface">(SDL_Surface *)</param>
        /// <param name="dstrect">(SDL_Rect *)</param>
        /// <returns></returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int SDL_LowerBlit(IntPtr srcSurface, IntPtr srcrect, IntPtr dstSurface, ref SDL_Rect dstrect);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="srcSurface">(SDL_Surface *)</param>
        /// <param name="srcrect">(SDL_Rect *)</param>
        /// <param name="dstSurface">(SDL_Surface *)</param>
        /// <param name="dstrect">(SDL_Rect *)</param>
        /// <returns></returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int SDL_LowerBlit(IntPtr srcSurface, ref SDL_Rect srcrect, IntPtr dstSurface, IntPtr dstrect);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="srcSurface">(SDL_Surface *)</param>
        /// <param name="srcrect">(SDL_Rect *)</param>
        /// <param name="dstSurface">(SDL_Surface *)</param>
        /// <param name="dstrect">(SDL_Rect *)</param>
        /// <returns></returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int SDL_LowerBlit(IntPtr srcSurface, ref SDL_Rect srcrect, IntPtr dstSurface, ref SDL_Rect dstrect);

        #endregion

        #region [SDL_LowerBlitScaled]

        /// <summary>
        /// 
        /// </summary>
        /// <param name="srcSurface">(SDL_Surface *)</param>
        /// <param name="srcrect">(SDL_Rect *)</param>
        /// <param name="dstSurface">(SDL_Surface *)</param>
        /// <param name="dstrect">(SDL_Rect *)</param>
        /// <returns></returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int SDL_LowerBlitScaled(IntPtr srcSurface, IntPtr srcrect, IntPtr dstSurface, IntPtr dstrect);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="srcSurface">(SDL_Surface *)</param>
        /// <param name="srcrect">(SDL_Rect *)</param>
        /// <param name="dstSurface">(SDL_Surface *)</param>
        /// <param name="dstrect">(SDL_Rect *)</param>
        /// <returns></returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int SDL_LowerBlitScaled(IntPtr srcSurface, IntPtr srcrect, IntPtr dstSurface, ref SDL_Rect dstrect);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="srcSurface">(SDL_Surface *)</param>
        /// <param name="srcrect">(SDL_Rect *)</param>
        /// <param name="dstSurface">(SDL_Surface *)</param>
        /// <param name="dstrect">(SDL_Rect *)</param>
        /// <returns></returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int SDL_LowerBlitScaled(IntPtr srcSurface, ref SDL_Rect srcrect, IntPtr dstSurface, IntPtr dstrect);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="srcSurface">(SDL_Surface *)</param>
        /// <param name="srcrect">(SDL_Rect *)</param>
        /// <param name="dstSurface">(SDL_Surface *)</param>
        /// <param name="dstrect">(SDL_Rect *)</param>
        /// <returns></returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern int SDL_LowerBlitScaled(IntPtr srcSurface, ref SDL_Rect srcrect, IntPtr dstSurface, ref SDL_Rect dstrect);

        #endregion

        #endregion

        #region [SDL_rwops.h]

        /// <summary>
        /// 
        /// </summary>
        /// <returns>(SDL_RWops *)</returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern IntPtr SDL_AllocRW();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="area">(SDL_RWops *)</param>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern void SDL_FreeRW(IntPtr area);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mem">(void *)</param>
        /// <param name="size"></param>
        /// <returns>(SDL_RWops *)</returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern IntPtr SDL_RWFromMem(IntPtr mem, int size);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mem">(void *)</param>
        /// <param name="size"></param>
        /// <returns>(SDL_RWops *)</returns>
        [DllImport(DLL, CallingConvention = CALL, CharSet = CHARSET, SetLastError = true)]
        public static extern unsafe IntPtr SDL_RWFromMem(void* mem, int size);
        #endregion
    }
}
