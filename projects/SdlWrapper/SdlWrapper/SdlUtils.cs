﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Runtime.InteropServices;

namespace SdlWrapper
{
    public static class SdlUtils
    {
        public static unsafe IntPtr CreateSurface(byte[] data)
        {
            IntPtr pRwops = IntPtr.Zero;
            IntPtr ret = IntPtr.Zero;
            try
            {
                fixed(void* p = data)
                {
                    int len = data.Length;
                    pRwops = Sdl.SDL_RWFromMem(p, len);
                    ret = SdlImage.IMG_Load_RW(pRwops, 1);
                    return ret;
                }
            }
            finally
            {
                Sdl.SDL_FreeRW(pRwops);
            }
        }

        public static unsafe Size GetSurfaceSize(IntPtr p)
        {
            if(p == IntPtr.Zero)
            {
                return new Size(0, 0);
            }
            else
            {
                Sdl.SDL_Surface* surface = (Sdl.SDL_Surface*)p;
                Size size = new Size(surface->w, surface->h);
                return size;
            }
        }
    }
}
